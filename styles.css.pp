#lang pollen
◊; /* flex overview: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox */

body {
    font-family: charterregular, serif ;
    font-size: 133%;
    background-color: #fffff8;
    color: #111111;
}

body > * {
  flex: 1 100%;
}

h1,h2,h3,h4,h5,h6,p {
 line-height: 1.55;
}

h1 { font-size: 4em; }
h2 { font-size: 3em; }
h3 { font-size: 2em; }


/* home */

#index-pages {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    column-gap: 3em;
}


ul .page {
    padding-bottom: 1.45em;
}

/* poem pages */

article {
    display: flex;
    flex-flow: column wrap;
    align-items: center;
    justify-content: start;
}

header {
    display: flex;
    flex-flow: column wrap;
    align-items: center;
    justify-content: start;
    max-width: 30em; /* one character ~ 0.5 em*/
}

.poem-strophe {
    line-height: 1.55;
    font-size: 1em;
    margin-bottom: 1.5em;
◊; maybe if we set max-width to some number of ems we can wrap lines when they get too long?
}

.poem-verse {
    /* we use display block so that text-indent works as expected */
    display: block;
    /* we need negative indentation because we need a way to visually
    indicate verses even when the viewport is too narrow; if we have
    none, it's difficult to know when a verse is over */
    text-indent: -1em;
    /* this makes space for the negative indent */
    padding-left: 1em;
◊; if we ever have problems with long lines, try:
◊; max-width: 30em ;  /* one character ~ 0.5 em*/
}

nav {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    column-gap: 3em;
}

.navigation {
    font-size: 1.8em;
}

#recording-container {
    padding-bottom: 2em;
}

◊; /* display player only when hovering, because it's ugly */
#recording-container #poem-recording {
    display: none;
}

#recording-container:hover #recording-show {
    display: none;
}

#recording-container:hover #poem-recording {
    display: block;
}

◊; /*
◊; Local Variables:
◊; mode: css
◊; comment-start: "◊; "
◊; indent-tabs-mode: nil
◊; End:
◊; */
